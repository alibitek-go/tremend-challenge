package main

import (
	"fmt"
	"testing"
)

type testcase struct {
	sortedInputArray       []int
	expectedTraversalOuput []int
}

func equalSlices(a, b []int) bool {
	if (a == nil) != (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}

func TestBinarySearchConstructionInOrder(t *testing.T) {
	testcases := []testcase{
		{
			sortedInputArray:       []int{},
			expectedTraversalOuput: nil,
		},
		{
			[]int{1, 2, 3, 4, 5, 6, 7},
			[]int{1, 2, 3, 4, 5, 6, 7},
		},
		{
			[]int{1, 2, 3, 4, 6, 7, 8, 9},
			[]int{1, 2, 3, 4, 6, 7, 8, 9},
		},
		{
			[]int{-10, -5, 0, 5, 10},
			[]int{-10, -5, 0, 5, 10},
		},
	}

	for _, testcase := range testcases {
		fmt.Printf("Testcase: %v\n", testcase)

		actualInorderTraversal := TreeInorderTraversal(buildBinarySearchTree(testcase.sortedInputArray))
		if !equalSlices(testcase.expectedTraversalOuput, actualInorderTraversal) {
			t.Errorf("Expected %v, Got %v", testcase.expectedTraversalOuput, actualInorderTraversal)
		}
	}
}

func TestBinarySearchConstructionPreOrder(t *testing.T) {
	testcases := []testcase{
		{
			sortedInputArray:       []int{},
			expectedTraversalOuput: nil,
		},
		{
			[]int{1, 2, 3, 4, 5, 6, 7},
			[]int{4, 2, 1, 3, 6, 5, 7},
		},
		{
			[]int{1, 2, 3, 4, 6, 7, 8, 9},
			[]int{6, 3, 2, 1, 4, 8, 7, 9},
		},
		{
			[]int{-10, -5, 0, 5, 9},
			[]int{0, -5, -10, 9, 5},
		},
	}

	for _, testcase := range testcases {
		fmt.Printf("Testcase: %v\n", testcase)

		actualPreorderTraversal := TreePreorderTraversal(buildBinarySearchTree(testcase.sortedInputArray))
		if !equalSlices(testcase.expectedTraversalOuput, actualPreorderTraversal) {
			t.Errorf("Expected %v, Got %v", testcase.expectedTraversalOuput, actualPreorderTraversal)
		}
	}
}
