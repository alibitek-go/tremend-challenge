# tremend-challenge

Starter code: https://play.golang.org/p/ejqOmRQdOde  

## Requirements
Sorted slice to binary search tree converter.  
In order traversal ordering should be the same as that of the sorted input slice.  

This is actually the min height BST construction problem.

### Example
Sorted input array: `1 5 6 7 10 12 13 14 20`  

Binary search tree representation:
```
M:10
  L:6
    L:5
      L:1
    R:7
  R:14
    L:13
      L:12
    R:20
```

In order traversal (Left, Root, Right) ouput is expected to be the same as input array.  


## Complexity analysis

### Recursive algorithm
See `buildBinarySearchTree` function inside binary_search.go  
Time: O(n)
Space: O(n)  
where n is the length of the array

### Iterative algorithm
TODO: Use a stack

## Dependencies
None. The Go standard library.  

## Usage
Run tests: `go test`  
Run main example: `go run binary_search.go`  
