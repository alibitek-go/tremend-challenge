package main

import (
	"fmt"
	"io"
	"os"
	"sort"
)

// The TreeNode represents a node in a binary tree.
type TreeNode struct {
	Value int
	Left  *TreeNode
	Right *TreeNode
}

// TreeInorderTraversal converts a binary tree data structure into a slice with elements in inorder traversal (Left, Root, Right)
func TreeInorderTraversal(root *TreeNode) []int {
	if root == nil {
		return nil
	}

	if root.Left == nil && root.Right == nil {
		return []int{root.Value}
	}

	// Left
	result := TreeInorderTraversal(root.Left)

	// Root
	result = append(result, root.Value)

	// Right
	result = append(result, TreeInorderTraversal(root.Right)...)

	return result
}

// TreePreorderTraversal converts a binary tree data structure into a slice with elements in preorder traversal (Root, Left, Right)
func TreePreorderTraversal(root *TreeNode) []int {
	if root == nil {
		return nil
	}

	if root.Left == nil && root.Right == nil {
		return []int{root.Value}
	}

	// Root
	result := []int{root.Value}

	// Left
	result = append(result, TreePreorderTraversal(root.Left)...)

	// Right
	result = append(result, TreePreorderTraversal(root.Right)...)

	return result
}

// TreePostOrderTraversal converts a binary tree data structure into a slice with elements in postorder traversal (Left, Right, Root)
func TreePostOrderTraversal(root *TreeNode) []int {
	if root == nil {
		return nil
	}

	if root.Left == nil && root.Right == nil {
		return []int{root.Value}
	}

	// Left
	result := TreePostOrderTraversal(root.Left)

	// Right
	result = append(result, TreePostOrderTraversal(root.Right)...)

	// Root
	result = append(result, root.Value)

	return result
}

// PrintTree prints a tree to the specified writer using inorder traversal.
func PrintTree(w io.Writer, root *TreeNode, numberOfSpaces int, label rune) {
	if root == nil {
		return
	}

	for i := 0; i < numberOfSpaces; i++ {
		fmt.Fprint(w, " ")
	}

	fmt.Fprintf(w, "%c:%v\n", label, root.Value)
	PrintTree(w, root.Left, numberOfSpaces+2, 'L')
	PrintTree(w, root.Right, numberOfSpaces+2, 'R')
}

func buildBinarySearchTree(sortedArray []int) *TreeNode {
	sortedArrayLength := len(sortedArray)

	if sortedArrayLength == 0 || !sort.SliceIsSorted(sortedArray, func(a, b int) bool { return a < b }) {
		return nil
	}

	if sortedArrayLength == 1 {
		return &TreeNode{Value: sortedArray[0]}
	}

	midIndex := sortedArrayLength / 2

	return &TreeNode{
		// Midpoint should be the root node in the (sub)tree
		Value: sortedArray[midIndex],

		// First half (aka numbers < midpoint) should be on the left subtree
		Left: buildBinarySearchTree(sortedArray[:midIndex]),

		// Second half (aka numbers > midpoint) should be on the right subtree
		Right: buildBinarySearchTree(sortedArray[midIndex+1:]),
	}
}

func main() {
	sortedArray := []int{1, 2, 3, 4, 5}
	tree := buildBinarySearchTree(sortedArray)
	fmt.Printf("Preorder traversal: %v\n", TreePreorderTraversal(tree))
	fmt.Printf("Inorder traversal: %v\n", TreeInorderTraversal(tree))
	fmt.Printf("Postorder traversal: %v\n", TreePostOrderTraversal(tree))
	PrintTree(os.Stdout, tree, 0, 'M')
}
